<?php
echo '<pre>';
$resultado = array();
if (isset($_FILES['nom_fichero'])) {
    $err = $_FILES['nom_fichero']['error'];
    if (!$err) {
        // cogemos la ruta local del archivo
        $rutalocal = $_FILES['nom_fichero']['tmp_name'];
        // recogemos el contenido del archivo
        $contenido = file_get_contents($rutalocal);
        // pasamos el texto a minuscula
        $contenido = strtolower($contenido);
        // cortamos el texto por los caracteres, -1 para poner de limite todo el texto, y PREG_SPLIT_NO_EMPTY para no devolver elementos vacios
        $arraypalabras = preg_split('/[\s!¿?\-\.,;:"\—\(\)\']+/', $contenido, -1, PREG_SPLIT_NO_EMPTY);
        // recorremos el array
        foreach ($arraypalabras as $palabra) {
            // si no esta la palabra como key en el array resultado
            if (!array_key_exists($palabra, $resultado)) {
                // metemos un nuevo objeto al array con key de la palabra y valor 1
                $resultado[$palabra] = 1;
            } else {
                // si existe sumamos 1 al valor
                $resultado[$palabra]+= 1;
            }
        }
    } else {
        echo "Ha habido un error al subir";
    }
    echo '<table>';
    if (isset($_FILES['nom_fichero'])) {
        echo "<tr><td>PALABRA</td><td>REPETICIONES</td></tr>";
        // arsort ordena el array en orden descendiente segun el valor
        arsort($resultado);
        foreach ($resultado as $key => $value) {
            // utf8_encode para que salgan bien los acentos, ñs, etc..
            echo "<tr><td>" . utf8_encode($key) . "</td><td class='num'>$value</td></tr>";
        }
    }

    echo '</table>';
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <style>
            td {
                border: 1px solid black;
                height: 30px;
            }
            .num {
                text-align: right;
            }
        </style>
    </head>
    <body>
        <form method="post" enctype="multipart/form-data">
            <input type="file" name="nom_fichero" /><br />
            <input type="submit" name="enviar" value="Enviar" />
        </form>
    </body>
</html>



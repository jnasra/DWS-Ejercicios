<?php
/*
  Para cada asignatura se mostrará:
  El número de aprobados y suspensos con % sobre total
  Una tabla de frecuencias de notas: Nota,número de alumnos,% sobre total
 */
//echo '<pre>';
$resultado = array();
if (isset($_FILES['nom_fichero'])) {
    $err = $_FILES['nom_fichero']['error'];
    if (!$err) {
        // ruta local del archivo
        $rutalocal = $_FILES['nom_fichero']['tmp_name'];

        // convertimos el archivo csv en un array
        $csv = array_map('str_getcsv', file($rutalocal));

        // recorremos el array y recogemos la asignatura y la nota, nos olvidamos del alumno
        foreach ($csv as $alumno) {
            $asignatura = $alumno[1];
            $nota = $alumno[2];
            // en un nuevo array creamos la asignatura si no existe y le asignamos un array con las notas como valor 
            if (!array_key_exists($asignatura, $resultado)) {
               $asig['notas'] = $nota;
               $resultado[$asignatura] = $asig;
                
            } else {
                // si existe metemos la nota en el array de notas para esa asignatura
                array_push($resultado[$asignatura], $nota);
            }
        }
    } else {
        echo "Ha habido un error al subir";
    }

    // recorremos el array con las asignaturas y sus notas
    foreach ($resultado as $key => $value) {
        $aprobados = 0;
        $suspensos = 0;
        // para cada asignatura calculamos los aprobados y suspensos
        foreach ($value as $nota) {
            if ($nota >= 5) {
                $aprobados+=1;
            } else {
                $suspensos+=1;
            }
        }
        // sacamos el procentaje total de aprobados y suspensos
        $aprobados = round(($aprobados / count($value)) * 100, 2);
        $suspensos = 100 - $aprobados;
        echo "<div class='asignatura'>";
        echo "<p>ASIGNATURA: $key<br>PORCENTAJE DE APROBADOS:$aprobados%<br>PORCENTAJE DE SUSPENSOS:$suspensos%</p>";
        echo '<table>';
        // creamos un nuevo array que cuenta la cantidad de cada nota de la asignatura
        $counts = array_count_values($value);
        // notas del 0 al 10
        for ($i = 0; $i < 11; $i++) {
            // condicion para que no de warning de apuntar a elementos nulos si nadie tiene esa nota en particular
            if (!isset($counts[$i])) {
                $counts[$i] = 0;
            }
            // sacamos el porcentaje de cada nota del total
            $porc = round(($counts[$i] / count($value)) * 100, 2);
            echo "<tr><td>Nota: $i</td><td class='textoright'>$counts[$i] alumnos</td><td class='textoright'>$porc % sobre el total</td></tr>";
        }
        echo '</table>';
        echo "</div>";
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <style>

            p {
                padding: 15px;
                font-weight: bold;
            }
            table {
                margin: 0 auto;
                padding: 15px;
            }
            td {
                border:1px solid black;
            }
            .asignatura {
                border:2px solid blue;
                width: 330px;
                margin: 5px;
                float: left;
            }
            .textoright {
                text-align: right;
            }
            form {
                margin: 30px;
            }
            input {
                margin: 5px;
            }
        </style>
    </head>
    <body>
        <form method="post" enctype="multipart/form-data">
            <input type="file" name="nom_fichero" /><br />
            <input type="submit" name="enviar" value="Enviar" />
        </form>
    </body>
</html>

<?php

require 'libfunciones.php';


if (getparam('accion')) {
    $errores = array();
    
    $nombre = getparam('nombre');
    if (strlen($nombre) < 3)
        $errores['nombre'] = ' Nombre muy corto';
    
    $edad = getparam('edad');
    if ($edad <= 0 || $edad > 100)
        $errores['edad'] = ' Edad no válida';
    
    $email = getparam('email');
    if (strpos($email, "@") === false)
        $errores['email'] = ' Correo electrónico no válido';
    
    $sexo = getparam('sexo');
    if (!$sexo)
        $errores['sexo'] = ' Dato requerido';

    $comentarios = getparam('comentarios');

    $password = getparam('password');
    if (strlen($password) < 6)
        $errores['password'] = ' Contraseña demasiado corta';
    
    $condiciones = getparam('condiciones');
    if ($condiciones != 1)
        $errores['condiciones'] = ' Marque la casilla de Aceptar';
    
    if (!count($errores)) {
        echo "<h2>DATOS CORRECTOS</h2><a href=?>Empezar</a>";
        die;
    }
} else {
    $nombre = '';
    $edad = '';
    $sexo = '';
    $email ='';
    $comentarios ='';
    $password ='';
    $errores = array();
    
}

require 'form_registro.php';

?>

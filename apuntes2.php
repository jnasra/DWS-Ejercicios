<?php

$k =1;

if ($k) {
    echo "k es true";
}

?>


/*
    si pones $k==="asd" hace un casting estricto
*/

<?php

$nombre="Manolo";

$busco='a';

$k = strpos($nombre,$busco);

if ($k) {
    echo "El nombre $nombre contiene $busco en la pos $k";
}

?>

<?php

$nombre="Manolo";

$busco='M';

$k = strpos($nombre,$busco);

if ($k !== false) {
    echo "El nombre $nombre contiene $busco en la pos $k";
}

?>

/*
    Como la posicion es 0 devolveria false entonces tenemos que poner un = de mas 
    en la comparacion para que haga casting estricto y no coga la posicion 0 como false
*/

<?php

$a=9;

$b = ++$a;

$k = isset($_GET['orden'])? "ORDENADO" : "SIN ORDENAR";

/*
 la expresion de arriba es igual a la de abajo
 *  */

if ($_GET['orden']) {
    $k="ORDENADO";
} else {
    $k="SIN ORDENAR";
}

if ($k !== false) {
    echo "El nombre $nombre contiene $busco en la pos $k";
}

?>